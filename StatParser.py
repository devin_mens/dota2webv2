'''
Dota 2 Web API stats parser using the Python wrapper created by github user andrewsnowden.
Wrapper project located at : https://github.com/andrewsnowden/dota2py
'''
__author__ = 'Devin Mens'
from dota2py import api
from CreateDumpTable import DataDump
import time

print(time.strftime("%H:%M:%S"))


api.set_api_key('DA80BE3C688D2030FD0B4A543B3493D0')


playerId64 = int(api.get_steam_id("drunkdutch")["response"]["steamid"])  # Convert account id to steam64 id
playerId = playerId64 - 76561197960265728  # Convert steam64 id to steam32 id used by dota 2


'''
function to sanitize records to remove unwanted key/data from source data
'''


def removeKey(dic,key):
    targetDict = dict(dic)
    if key in dic.keys():
        del targetDict[key]
    return targetDict

'''
Main class to pull and format player specific match data
'''


class DotaPull():
    def __init__(self, playerId):
        self._playerId = playerId

    '''
    Core function to retrieve data from api calls, parse and sanitize before inserting into player stats database
    '''
    def playerStats(self, games=100):
        dotaParser = DataDump(self._playerId)
        req, left = divmod(games,100)
        current = 0
        matches = []
        gameId = 0
        '''
        Simple loop to chunk user desired game count in chunks based on api limits and appending into one return list
        '''
        while current <= req:
            if current == 0:
                matches.extend((api.get_match_history(account_id=self._playerId, matches_requested=100)['result']['matches']))
                gameId = matches[(len(matches)-1)].get('match_id')
                current += 1
            else:
                matches.extend((api.get_match_history(start_at_match_id=gameId-1, account_id=self._playerId, matches_requested=100)['result']['matches']))
                gameId = matches[(len(matches)-1)].get('match_id')
                current += 1
        if left != 0:
            matches.extend((api.get_match_history(start_at_match_id=gameId-1, account_id=self._playerId, matches_requested=left)['result']['matches']))
        '''
        Loop through returned games and return only json for user stats
        Sanitize data for sql insertion and reformat for our uses
        Ends with call to sqlite function to insert to summary table from temp dump table
        '''
        for index, match in enumerate(matches):
            matchGet = api.get_match_details(matches[index]["match_id"])
            for player in matchGet['result']['players']:
                accountId = player.get('account_id')
                if accountId == playerId and player.get('leaver_status') != 1:
                    targetId = player
                    targetId['match_id'] = matchGet['result']['match_id']
                    targetId['win'] = matchGet['result']['radiant_win']
                    targetId['duration'] = matchGet['result']['duration']
                    if targetId['player_slot'] >= 100 and targetId['win'] == False:
                        targetId['win'] = True
                    sanDict = removeKey(targetId, 'ability_upgrades')
                    saniDict = removeKey(sanDict, 'additional_units')
                    sortedData = [value for (key, value) in sorted(saniDict.items())]
                    dotaParser.dotaParse(sortedData)
        dotaParser.dumpMerge()

    def heroParse(self):
        dotaParser = DataDump(self._playerId)
        dotaParser.heroParse()
    def heroStats(self):
        dotaParser = DataDump(self._playerId)
        dotaParser.heroStats()

print(playerId)
player = DotaPull(playerId)
gamesCount = 700
# player.playerStats(gamesCount)
# player.heroParse()
player.heroStats()
print(time.strftime("%H:%M:%S"))





