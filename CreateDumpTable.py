'''
Created on Dec 22, 2014

@author: Devin
'''
import sqlite3
import os
from dota2py import api
api.set_api_key('DA80BE3C688D2030FD0B4A543B3493D0')
heroList = api.get_heroes()['result']['heroes']
heroes = []
for index, hero in enumerate(heroList):
    heroes.append(heroList[index]['id'])


class DataDump():
    conn = None
    c = None

    def __init__(self, playerId):
        self._playerId = playerId
        self.conn = sqlite3.connect(str(self._playerId) + '.db', )
        self.c = self.conn.cursor()
        self.c.execute('''create table if not exists Summary
                       (account_id integer,
                        assists integer,
                        deaths integer,
                        denies integer,
                        duration integer,
                        gold integer,
                        gold_per_min integer,
                        gold_spent integer,
                        hero_damage integer,
                        hero_healing integer,
                        hero_id integer,
                        item_0 integer,
                        item_1 integer,
                        item_2 integer,
                        item_3 integer,
                        item_4 integer,
                        item_5 integer,
                        kills integer,
                        last_hits integer,
                        leaver_status integer,
                        level integer,
                        match_id integer primary key,
                        player_slot integer,
                        tower_damage integer,
                        win integer,
                        xp_per_min integer)''')
        self.c.execute('''create table if not exists Player
                        (games_played integer,
                         win_rate real,
                         radiant_win real,
                         dire_win real
                         short_duration integer,
                         short_win real,
                         mid_duration integer,
                         mid_win real,
                         long_duration integer,
                         long_win real)''')

        for value in heroes:
            tableString = ('''create table if not exists %s
                           (account_id integer,
                            assists integer,
                            deaths integer,
                            denies integer,
                            duration integer,
                            gold integer,
                            gold_per_min integer,
                            gold_spent integer,
                            hero_damage integer,
                            hero_healing integer,
                            hero_id integer,
                            item_0 integer,
                            item_1 integer,
                            item_2 integer,
                            item_3 integer,
                            item_4 integer,
                            item_5 integer,
                            kills integer,
                            last_hits integer,
                            leaver_status integer,
                            level integer,
                            match_id integer primary key,
                            player_slot integer,
                            tower_damage integer,
                            win integer,
                            xp_per_min integer)''' % ('`'+str(value)+"`"))
            self.c.execute(tableString)

    def dotaParse(self, dotaStats):
        self.c.execute('''create table if not exists gameDump
          (account_id integer,
           assists integer,
           deaths integer,
           denies integer,
           duration integer,
           gold integer,
           gold_per_min integer,
           gold_spent integer,
           hero_damage integer,
           hero_healing integer,
           hero_id integer,
           item_0 integer,
           item_1 integer,
           item_2 integer,
           item_3 integer,
           item_4 integer,
           item_5 integer,
           kills integer,
           last_hits integer,
           leaver_status integer,
           level integer,
           match_id integer primary key,
           player_slot integer,
           tower_damage integer,
           win integer,
           xp_per_min integer)''')
        self.c.execute('INSERT INTO gameDump values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', dotaStats)
        self.conn.commit()

    def dumpMerge(self):
        self.c.execute('INSERT OR REPLACE INTO Summary SELECT * FROM gameDump')
        self.c.execute('''drop table if exists gameDump''')
        self.conn.commit()

    def heroParse(self):
        for value in heroes:
            parseString = ('INSERT OR REPLACE INTO %s SELECT * FROM Summary WHERE hero_id=%s' % (('`'+str(value)+'`'), value))
            self.c.execute(parseString)
            self.conn.commit()

    def heroStats(self):
        hero_stats=[]
        for value in heroes:
            winString = ("SELECT sum(win) FROM "+('`'+str(value)+'`'))
            self.c.execute(winString)
            winCount = self.c.fetchone()
            wins = winCount[0]
            sqlString = ("SELECT count(match_id) FROM "+('`'+str(value)+'`'))
            self.c.execute(sqlString)
            rows = self.c.fetchone()
            games = rows[0]
            if games != 0:
                win_rate = wins/games
                heroWin = {"id": value, "win_rate": win_rate, "games":games}
                hero_stats.append(heroWin)
        for hero in hero_stats:
            print(hero['id'],hero['win_rate'],hero['games'])

